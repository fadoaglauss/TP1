PARAMETROS= -lglut -lGLU -lGL -lm -lSOIL `sdl-config --cflags --libs` -lSDL_mixer -w
PARAMETROSGTK= `pkg-config --cflags --libs gtk+-3.0` -w
all: teste.exe
#teste.exe: topSecret.o main.c
#	gcc -o $@ $^ $(PARAMETROS)
#topSecret.o: topSecret.c
#	gcc -o $@ -c $^ $(PARAMETROS)
#teste.exe: topSecret.c
#	gcc -o $@ $^ $(PARAMETROS)
teste.exe: topSecret.c
	gcc -o $@ $^ $(PARAMETROS) $(PARAMETROSGTK)
clean:
	rm -rf *.o
mrproper: clean
	rm -rf teste.exe
#Duvidas olhar em pt.wikibooks.org/wiki/Programar_em_C/Makefiles
