#include<GL/glew.h>
#include<GL/freeglut.h>
#include<SOIL/SOIL.h>
#include<stdio.h>
#include <SDL/SDL.h>
#include <SDL/SDL_audio.h>
#include <SDL/SDL_mixer.h>
#include <gtk/gtk.h>
//SDL_Surface *screen;
Mix_Music *musicMenu0 = NULL;
char *nomedoarquivo;
//GLuint textureTopSecret0;
//GLuint texturePressSpace0;
//GLuint textureBackgroundMenu0;
//GLuint textureButton0Menu0; GLuint textureButton1Menu0; GLuint textureButton2Menu0; GLuint textureButton3Menu0;
void criar();
typedef struct {
	int buttonvertex_0y, buttonvertex_1y;
} ButtonOfMenu1;
ButtonOfMenu1 button[4];	
void initMenu0(GLuint *textureButton0Menu0,GLuint *textureButton1Menu0,GLuint *textureButton2Menu0,GLuint *textureButton3Menu0,GLuint *textureBackgroundMenu0){	 
	*textureBackgroundMenu0 = SOIL_load_OGL_texture("backgroundMenu0.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	*textureButton0Menu0 = SOIL_load_OGL_texture("exit.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	*textureButton1Menu0 = SOIL_load_OGL_texture("highScore.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	*textureButton2Menu0 = SOIL_load_OGL_texture("howToPlay.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	*textureButton3Menu0 = SOIL_load_OGL_texture("start.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
}

void initMusicMenu0(){
	Mix_OpenAudio( 22050, MIX_DEFAULT_FORMAT, 2, 1024 );
	 atexit(Mix_CloseAudio);
         musicMenu0 = Mix_LoadMUS("hail_to_the_chief.mp3");
}


void Menu0(GLuint *textureButton0Menu0,GLuint *textureButton1Menu0,GLuint *textureButton2Menu0,GLuint *textureButton3Menu0,GLuint *textureBackgroundMenu0){
	initMusicMenu0();
	Mix_VolumeMusic(100);
    	Mix_PlayMusic(musicMenu0, -1);
    	
	glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, *textureBackgroundMenu0);
			glBegin(GL_TRIANGLE_FAN);
				glTexCoord2f(0, 0); glVertex2f(0,0); glTexCoord2f(1, 0); glVertex2f(3840,0); glTexCoord2f(1, 1); glVertex2f(3840,2160); glTexCoord2f(0, 1); glVertex2f(0,2160); 
			glEnd();
		glBindTexture(GL_TEXTURE_2D, *textureButton0Menu0);
			glBegin(GL_TRIANGLE_FAN);
	      			glTexCoord2f(0, 0); glVertex3f(3000, button[0].buttonvertex_0y, 0); glTexCoord2f(1, 0); glVertex3f(3600, button[0].buttonvertex_0y, 0); glTexCoord2f(1, 1); glVertex3f(3600, button[0].buttonvertex_1y, 0); glTexCoord2f(0, 1); glVertex3f(3000, button[0].buttonvertex_1y, 0);
	   		glEnd();
		glBindTexture(GL_TEXTURE_2D, *textureButton1Menu0);
			glBegin(GL_TRIANGLE_FAN);
	      			glTexCoord2f(0, 0); glVertex3f(3000, button[1].buttonvertex_0y, 0); glTexCoord2f(1, 0); glVertex3f(3600, button[1].buttonvertex_0y, 0); glTexCoord2f(1, 1); glVertex3f(3600, button[1].buttonvertex_1y, 0); glTexCoord2f(0, 1); glVertex3f(3000, button[1].buttonvertex_1y, 0);
	   		glEnd();
		glBindTexture(GL_TEXTURE_2D, *textureButton2Menu0);
			glBegin(GL_TRIANGLE_FAN);
	      			glTexCoord2f(0, 0); glVertex3f(3000, button[2].buttonvertex_0y, 0); glTexCoord2f(1, 0); glVertex3f(3600, button[2].buttonvertex_0y, 0); glTexCoord2f(1, 1); glVertex3f(3600, button[2].buttonvertex_1y, 0); glTexCoord2f(0, 1); glVertex3f(3000, button[2].buttonvertex_1y, 0);
	   		glEnd();
		glBindTexture(GL_TEXTURE_2D, *textureButton3Menu0);
			glBegin(GL_TRIANGLE_FAN);
	      			glTexCoord2f(0, 0); glVertex3f(3000, button[3].buttonvertex_0y, 0); glTexCoord2f(1, 0); glVertex3f(3600, button[3].buttonvertex_0y, 0); glTexCoord2f(1, 1); glVertex3f(3600, button[3].buttonvertex_1y, 0); glTexCoord2f(0, 1); glVertex3f(3000, button[3].buttonvertex_1y, 0);
	   		glEnd();
	glDisable(GL_TEXTURE_2D);
	glFlush();
}


void drawMenu0(){
	GLuint textureButton0Menu0, textureButton1Menu0, textureButton2Menu0, textureButton3Menu0, textureBackgroundMenu0;
	glClear(GL_COLOR_BUFFER_BIT);
 	initMenu0(&textureButton0Menu0, &textureButton1Menu0, &textureButton2Menu0, &textureButton3Menu0, &textureBackgroundMenu0);
 	Menu0(&textureButton0Menu0, &textureButton1Menu0, &textureButton2Menu0, &textureButton3Menu0, &textureBackgroundMenu0);
	criar();
}

void creatRetangleButton(){
	button[0].buttonvertex_0y=750;
	button[0].buttonvertex_1y=890;
	for(int i=1; i<4;i++){
		button[i].buttonvertex_0y=button[i-1].buttonvertex_0y+350;
		button[i].buttonvertex_1y=button[i-1].buttonvertex_1y+350;
	}
}

void initLoadingScreen(GLuint *textureTopSecret0,GLuint *texturePressSpace0){
	*textureTopSecret0 = SOIL_load_OGL_texture("topSecret.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	*texturePressSpace0 = SOIL_load_OGL_texture("pressSpace.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
}

void loadingScreen(GLuint *textureTopSecret0,GLuint *texturePressSpace0){
	glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, *textureTopSecret0);
			glBegin(GL_TRIANGLE_FAN);
				glTexCoord2f(0, 0); glVertex2f(1320,200); 
                                glTexCoord2f(1, 0); glVertex2f(2520,200); 
                                glTexCoord2f(1, 1); glVertex2f(2520,400); 
                                glTexCoord2f(0, 1); glVertex2f(1320,400);
			glEnd();
		glBindTexture(GL_TEXTURE_2D, *texturePressSpace0);
			glBegin(GL_TRIANGLE_FAN);
				glTexCoord2f(0, 0); glVertex2f(1830,160); 
                                glTexCoord2f(1, 0); glVertex2f(2010,160); 
                                glTexCoord2f(1, 1); glVertex2f(2010,200);	
                                glTexCoord2f(0, 1); glVertex2f(1830,200);
			glEnd();
	glDisable(GL_TEXTURE_2D);
	glFlush();
}



void drawLoadingScreen(void){
	GLuint textureTopSecret0, texturePressSpace0;
	glClear(GL_COLOR_BUFFER_BIT);
	initLoadingScreen(&textureTopSecret0, &texturePressSpace0);
	loadingScreen(&textureTopSecret0, &texturePressSpace0);
}



void startProgram(void){
	glClearColor(0, 0, 0, 0); 
}

// Callback de redimensionamento
void rezise(int w, int h){
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, 3840, 0, 2160, -1, 1);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}
void highScore(){
	glClear(GL_COLOR_BUFFER_BIT);
}
void howToPlay(){
	glClear(GL_COLOR_BUFFER_BIT);
}
void Start(){
	glClear(GL_COLOR_BUFFER_BIT);
}

//Callback de evento de mouse É RELATIVO A JANEL E NAÕ AO MOUSE, ISSO É PROBLEMA!!
void mouseMenu0(int button, int state, int x, int y){
	if(button==GLUT_LEFT_BUTTON){
		if (state == GLUT_DOWN){
			if(x>1000 && x<1200){
				if(y>73 && y<120){Start();
				}else if(y>190 && y<236){ howToPlay();
				}else if(y>306 && y<353){ highScore();
				}else if(y>423 && y<470){ exit(0);
				}
			}
		}
	}
}
//GTK functions

void salvar(GtkWidget *widget,gpointer data){
	printf("Salvo!!");
}
static void destroy(GtkWidget *widget,gpointer data){
	gtk_main_quit();
}
void loopGtk(){
	gtk_main_iteration_do(FALSE);
}
void criar(){
	GtkBuilder *gtkBuilder = gtk_builder_new();
	gtk_builder_add_from_file(gtkBuilder,"interface.glade", NULL);
	GtkWidget *botao = GTK_WIDGET(gtk_builder_get_object(gtkBuilder,"button1"));//gtk_button_new_with_label("Salvar");
	g_signal_connect(botao, "clicked", G_CALLBACK(salvar),NULL);
	GtkWidget *scoreWindow = GTK_WIDGET(gtk_builder_get_object(gtkBuilder,"window1"));//gtk_window_new(GTK_WINDOW_TOPLEVEL);
	g_signal_connect(scoreWindow,"destroy",G_CALLBACK(destroy),NULL);
	g_signal_connect_swapped(botao, "clicked", G_CALLBACK(gtk_widget_destroy),scoreWindow);
	//gtk_container_add(GTK_CONTAINER(scoreWindow),botao);
	//gtk_widget_show(botao);
	g_object_unref(G_OBJECT(gtkBuilder));
	gtk_widget_show_all(scoreWindow);
	glutIdleFunc(loopGtk);


}


// Callback de evento de teclado

void keyboard(unsigned char key, int x, int y){
	switch(key){
		case 27:	exit(0); break;
		case 32: drawMenu0(); break;
	}
}

int main(int argc, char **argv){

        SDL_Init(SDL_INIT_AUDIO);
	atexit(SDL_Quit);
    	nomedoarquivo = argv[0];

	//Inicializa o GLUT e define a versão do OpenGL
	glutInit(&argc, argv);
	glutInitContextVersion(1, 1);
	glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);
	//Configuração inicial da janela
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA);
	glutInitWindowSize(1280,720);
	glutInitWindowPosition(320, 150);
	//Inicialização da janela
	glutCreateWindow("TOP SECRET");
	
	creatRetangleButton();	

	//Inicialização da janela
	glutDisplayFunc(drawLoadingScreen);
	glutReshapeFunc(rezise);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouseMenu0);
	startProgram();
	//Universo Gtk
	gtk_init(&argc,&argv);


	
	glutMainLoop();
	return 0;
}
