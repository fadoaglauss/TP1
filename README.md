TP1
===

Trabalho de Computação gráfica do curso Engenharia de Computação do Centro Federal de Educação Tecnológica de Minas Gerais.

git instructions:
-----------------

1. git init .
2. git remote add origin https://github.com/fadoaglauss/TP1.git
3. git pull origin master
4. git add *arquivo a ser inserido*
5. git commit -m "*descrição das mudanças*"
6. (optional) git checkout -b *nome da nova branch*
7. (com 6) git push origin *nome da nova branch*
8. (sem 6) git push origin master
9. Well Done!

